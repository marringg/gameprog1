﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light : MonoBehaviour
{
    private Vector3 startPoint;
    private Vector3 endPoint;
    Vector3 currentPos;

    // Start is called before the first frame update
    GameObject dark;
    GameManager manager;
    void Start()
    {
        dark = GameObject.FindGameObjectWithTag("Darkness");

        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        manager = obj.GetComponent<GameManager>();

        startPoint = transform.position;
        currentPos = startPoint;
        endPoint = startPoint + new Vector3(0, .5f, 0);

    }

    // Update is called once per frame
    void Update()
    {
        currentPos = Vector3.MoveTowards(currentPos, endPoint, .25f * Time.deltaTime);
        transform.position = currentPos;
        if (currentPos == endPoint)
        {
            //Switch end and start so the sprite changes movement direction
            Vector3 temp = startPoint;
            startPoint = endPoint;
            endPoint = temp;
        }

        if (Input.GetButton("Power1"))
        {
            if(manager.metApollo)
            {
                GetComponent<SpriteRenderer>().enabled = true;
                if (dark != null)
                {
                    dark.GetComponent<SpriteRenderer>().enabled = false;
                }
            }
        } 
    }
}
