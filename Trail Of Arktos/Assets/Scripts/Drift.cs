﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drift : MonoBehaviour
{
    SpriteRenderer sr;
    public float moveSpeed; 
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < -29)
            transform.position = new Vector3(30, transform.position.y, 0);

        transform.Translate(moveSpeed * Time.deltaTime, 0, 0, Space.World);
    }
}
