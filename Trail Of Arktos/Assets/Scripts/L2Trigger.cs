﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class L2Trigger : MonoBehaviour
{
    GameManager manager;
    int scene;
    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        manager = obj.GetComponent<GameManager>();
        scene = SceneManager.GetActiveScene().buildIndex;

    }

    void OnTriggerEnter2D(Collider2D collision)
    { 
       
        manager.triggered = true;
        if (scene == 1)
        {
            manager.nextLevel = 3;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        manager.triggered = false;
        manager.nextLevel = scene;
    }
}
