﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : Collectables
{
    private Vector3 startPoint;
    private Vector3 endPoint;
    Vector3 currentPos;

    private GameManager manager;

    public bool interaction = false;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        manager = obj.GetComponent<GameManager>();
       


        startPoint = transform.position;
        currentPos = startPoint;
        endPoint = startPoint + new Vector3(0, 1, 0);
    }

    // Update is called once per frame
    void Update()
    {
        currentPos = Vector3.MoveTowards(currentPos, endPoint, 1 * Time.deltaTime);
        transform.position = currentPos;
        if (currentPos == endPoint)
        {
            //Switch end and start so the sprite changes movement direction
            Vector3 temp = startPoint;
            startPoint = endPoint;
            endPoint = temp;
        }

        if (interaction == true)
        {
            manager.boltCount++;
            GetComponent<SpriteRenderer>().enabled = false;
            interaction = false;

        }

    }
}
