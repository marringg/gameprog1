﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighting : MonoBehaviour
{
    public GameObject fire;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartFire()
    {
        GameObject obj = Instantiate<GameObject>(fire);
        Destroy(this.gameObject);
        Destroy(obj, 2);
    }
}
