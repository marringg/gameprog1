﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Dialogue : MonoBehaviour
{
    public TextMeshProUGUI textBox;
    public string[] sentences;
    private int index;
    public float typingSpeed;
    public bool interaction = false;


    void Start()
    {
        index = 0;
       
    }

    void Update()
    {
        if (interaction)
        {
            if (Input.GetButtonUp("Interaction"))
            {
                interaction = false;
                
                StartCoroutine(Text());
            }
        }
    }

    IEnumerator Text()
    {
       foreach (char letter in sentences[index])
       {
            textBox.text += letter;
            yield return new WaitForSeconds(typingSpeed);
       }
       yield return new WaitForSecondsRealtime(3);
       NextLine();

    }
    
    public void NextLine()
    {
        textBox.text = "";
        if(index < sentences.Length - 1)
        {
            index++;
            StartCoroutine(Text());
        }
    }
}
