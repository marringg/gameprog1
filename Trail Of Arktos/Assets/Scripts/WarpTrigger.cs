﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class WarpTrigger : MonoBehaviour
{
    public int targetLevel;
	public Vector3 targetLocation;

	Arktos player;
    void Start()
    {
		GameObject obj = GameObject.FindGameObjectWithTag("Player");
		if (obj != null)
			player = obj.GetComponent<Arktos>();
	}

    void Update()
    {
        
    }
	public void OnTriggerEnter2D(Collider2D hit)
	{
		if (hit.GetComponent<Arktos>() != null)
		{
			Debug.Log("Trigger");
			GameManager manager = GameManager.Instance;
			manager.triggered = true;
			manager.nextLevel = targetLevel;
			manager.returned = true;
			manager.playerLoc = targetLocation;
		}
	}
	public void OnTriggerExit2D(Collider2D hit)
	{
		if (hit.GetComponent<Arktos>() != null)
		{
			GameManager manager = GameManager.Instance;
			manager.triggered = false;
			manager.returned = false;
		}
	}
}
