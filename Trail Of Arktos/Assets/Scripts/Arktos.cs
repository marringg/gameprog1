﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arktos : MonoBehaviour
{
    private float speed = 3;
    public float jumpSpeed = 13;
    public bool isjump = false;
    public bool doubleJump = false;
    public Vector3 spawnPoint;

    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        //transform.position = spawnPoint;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float axis = Input.GetAxis("Horizontal");
        float hSpeed = Mathf.Abs(axis);

        //Move the sprite according to the input
        transform.Translate(new Vector3(axis * speed * Time.deltaTime, 0f, 0f));

        //Demetmine which way the sprite faces
        if (axis > 0.1)
            GetComponent<SpriteRenderer>().flipX = false;
        else if (axis < -0.1)
            GetComponent<SpriteRenderer>().flipX = true;

        animator.SetFloat("Speed", hSpeed);

        if (Input.GetButtonUp("Jump") == true || doubleJump)
        {
            if (isjump == false)
            {
                isjump = true;
                doubleJump = false;
                GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpSpeed;
            }
        }

        //Later this will be used to change levels/scenes
        Vector3 pos = transform.position;
        if (pos.x > 25)
            pos.x = 25;

        if (pos.x < -25)
            pos.x = -25;

        transform.position = pos;

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        isjump = false;
    }
}
