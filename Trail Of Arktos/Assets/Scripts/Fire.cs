﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public Sprite burned;
    private GameObject burnableTree;

    private void Start()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("Burnable");
        if (obj != null)
            burnableTree = obj;
    }
 
    public void SwapSprite()
    {
        if (burnableTree != null)
        {
            SpriteRenderer sr = burnableTree.GetComponent<SpriteRenderer>();
            sr.sprite = burned;
        }
    }
}
