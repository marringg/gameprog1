﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthenaPower : MonoBehaviour
{
    public GameObject[] hiddenObjects;
    GameManager manager;
    // Start is called before the first frame update
    void Start()
    { 
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        manager = obj.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Power2"))
        {
            if (manager.metAthena)
            {
               for(int i=0; i<hiddenObjects.Length;  i++)
                {
                    hiddenObjects[i].GetComponent<SpriteRenderer>().sortingOrder = 3;
                    GameObject obj = Instantiate<GameObject>(hiddenObjects[i]);
                    hiddenObjects[i].GetComponent<SpriteRenderer>().sortingOrder = 0;
                    Destroy(obj, 2);                    
                }
            }
        }
    }
}
