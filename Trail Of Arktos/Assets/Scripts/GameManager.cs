﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Make this the thing thats saved between levels
    //Empty list that holds the 
    private Arktos player;
    private Lyre apolloInteraction, apollo, apollo2;
    private Olive athenaInteraction, athena, athena2;
    private Bolt zeusInteraction, zeus;
    private Caduceus hermesInteraction, hermes;
    private Dialogue dialogue;

    public bool metApollo = false;
    public bool metAthena = false;
    public bool metHermes = false;
    public bool metZeus = false;
    public int lyreCount, oliveCount, boltCount, caduceusCount;

    public GameObject apolloPower, zeusPower, ExitTrigger;
    private GameObject burnableTree;

    public Sprite burnedTree;
    public bool burned;

    private bool exitOpen = false;
    public bool triggered;
    public int nextLevel;
    public bool returned;
    public bool start; 
    public Vector3 playerLoc;

    List<Collectables> collected;
    public bool collect;
    AudioSource audio;

    private GameObject obj;

    private static GameManager mSingleton;
    public static GameManager Instance
    {
        get { return mSingleton; }
    }
    // set your singleton if it hasn't been created, 
    // otherwise destroy the object
    void Awake()
    {
        // if the singleton is null, it means it's the 
        // first time we created this class
       
        if (mSingleton == null)
        {
            mSingleton = this;
            // the whole point of making a singleton is 
            // to create something that can't be destroyed
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            // otherwise, we've already set it, 
            // so destroy this object
            Destroy(gameObject);
        }
    }
    void Start()
    {
        Setup();
        collected = new List<Collectables>();
    }

    void OnLevelWasLoaded()
    {
        Setup();
        exitOpen = false;
        Collectables[] collectables = FindObjectsOfType(typeof(Collectables)) as Collectables[];

        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            Startup();
        }

        if (returned)
        {
            returned = false;
            player.transform.position = playerLoc;
        }

        if (burned)
        {
            if (burnableTree != null)
            {
                SpriteRenderer sr = burnableTree.GetComponent<SpriteRenderer>();
                sr.sprite = burnedTree;
            }
        }

        foreach(Collectables obj in collectables)
        {
            Debug.Log("Object = " + obj.name);
        }

        if (collected != null)
        {
            for (int i = 0; i < collected.Count; i++)
            {
                foreach (Collectables obj in collectables)
                {
                    if(obj.ID == collected[i].ID )
                    {
                        Debug.Log("Destorying");
                        Destroy(obj.gameObject);
                    }
                }
            }
        }
    }


    void Update()
    {

        if(SceneExit() && !exitOpen)
        {
            exitOpen = true;
            GameObject obj = Instantiate<GameObject>(ExitTrigger);
        }

        if (triggered)
        {
            if(Input.GetButtonUp("Interaction"))
            {
                triggered = false;
                SceneManager.LoadScene(nextLevel);
            }
        }

        if (apolloInteraction != null)
        {
            if (Mathf.Abs(player.transform.position.x - apolloInteraction.transform.position.x) < 0.5)
            {
                if (Input.GetButtonUp("Interaction"))
                {
                    metApollo = true;
                    apolloInteraction.interaction = true;
                    dialogue.interaction = true;
                    collected.Add(apolloInteraction);
                    collect = true;
                }
            }
        }

        if (metApollo)
        {
            if (apollo != null)
            {
                if (Mathf.Abs(player.transform.position.x - apollo.transform.position.x) < 0.5 && Mathf.Abs(player.transform.position.y - apollo.transform.position.y) < 0.5)
                {
                    if (Input.GetButtonUp("Interaction"))
                    {
                        apollo.interaction = true;
                        collected.Add(apollo);
                        collect = true;
                    }
                }
            }

            if(apollo2 != null)
            {
                if (Mathf.Abs(player.transform.position.x - apollo2.transform.position.x) < 0.5 && Mathf.Abs(player.transform.position.y - apollo2.transform.position.y) < 0.5)
                {
                    if (Input.GetButtonUp("Interaction"))
                    {
                        apollo2.interaction = true;
                        collected.Add(apollo2);
                        collect = true;
                    }
                }
            }

            if (Input.GetButton("Power1"))
            {              
               GameObject obj = Instantiate<GameObject>(apolloPower);
                Destroy(obj, 20);
            }
            
        }

        if (athenaInteraction != null)
        {
            if (Mathf.Abs(player.transform.position.x - athenaInteraction.transform.position.x) < 0.5)
            {
                if (Input.GetButtonUp("Interaction"))
                {
                    metAthena = true;
                    athenaInteraction.interaction = true;
                    dialogue.interaction = true;
                    collected.Add(athenaInteraction);
                    collect = true;
                }
            }
        }

        if (metAthena)
        {
            if (athena != null)
            {
                if (Mathf.Abs(player.transform.position.x - athena.transform.position.x) < 0.5 && Mathf.Abs(player.transform.position.y - athena.transform.position.y) < 0.5)
                {
                    if (Input.GetButtonUp("Interaction"))
                    {
                        athena.interaction = true;
                        collected.Add(athena);
                        collect = true;
                    }
                }
            }

            if (athena2 != null)
            {
                if (Mathf.Abs(player.transform.position.x - athena2.transform.position.x) < 0.7 && Mathf.Abs(player.transform.position.y - athena2.transform.position.y) < 0.7)
                {
                    if (Input.GetButtonUp("Interaction"))
                    {
                        athena2.interaction = true;
                        collected.Add(athena2);
                        collect = true;
                    }
                }
            }

        }

        if (hermesInteraction != null)
        {
            if (Mathf.Abs(player.transform.position.x - hermesInteraction.transform.position.x) < 0.5)
            {
                if (Input.GetButtonUp("Interaction"))
                {
                    metHermes = true;
                    hermesInteraction.interaction = true;
                    dialogue.interaction = true;
                    Debug.Log(dialogue.interaction);
                    collected.Add(hermesInteraction);
                    collect = true;
                }
            }
        }

        if (metHermes)
        {
            if (hermes != null)
            {
                if (Mathf.Abs(player.transform.position.x - hermes.transform.position.x) < 0.5 && Mathf.Abs(player.transform.position.y - hermes.transform.position.y) < 0.5)
                {
                    if (Input.GetButtonUp("Interaction"))
                    {
                        hermes.interaction = true;
                        collected.Add(hermes);
                        collect = true;
                    }
                }
            }

            if (Input.GetButtonUp("Power3"))
            {
                player.isjump = false;
                player.doubleJump = true;
            }

        }

        if (zeusInteraction != null)
        {
            if (Mathf.Abs(player.transform.position.x - zeusInteraction.transform.position.x) < 0.5)
            {
                if (Input.GetButtonUp("Interaction"))
                {
                    metZeus = true;
                    zeusInteraction.interaction = true;
                    dialogue.interaction = true;
                    collected.Add(zeusInteraction);
                    collect = true;
                }
            }
        }

        if (metZeus)
        {
            if (zeus != null)
            {
                if (Mathf.Abs(player.transform.position.x - zeus.transform.position.x) < 0.5 && Mathf.Abs(player.transform.position.y - zeus.transform.position.y) < 0.5)
                {
                    if (Input.GetButtonUp("Interaction"))
                    {
                        zeus.interaction = true;
                        collected.Add(zeus);
                        collect = true;
                    }
                }
            }

            if (Input.GetButtonUp("Power4"))
            {
                GameObject obj = Instantiate<GameObject>(zeusPower);
                burned = true;
            }


        }


        if (collect)
        {
            collect = false;
            audio = GetComponent<AudioSource>();
            audio.Play();
        }

    }

    public bool SceneExit()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        if (scene == 1)
        {
            if (lyreCount >= 3)
            {
                return true;
            }
        }
        else if(scene == 3)
        {
            if(lyreCount >=5 && oliveCount >= 3)
            {
                nextLevel = 4;
                return true;
            }
        }
        else if(scene == 4)
        {
            if(caduceusCount >= 2 && oliveCount >= 4 && lyreCount >=6)
            {
                nextLevel = 5;
                return true;
            }
        }
        else if (scene == 5)
        {
            if (boltCount >= 3 && caduceusCount >= 3 && oliveCount >= 5 && lyreCount >= 7)
            {
                nextLevel = 7;
                return true;
            }
        }
        else if (scene == 7)
        {
            if (boltCount >= 4 && caduceusCount >= 4 && oliveCount >= 6 && lyreCount >= 8)
            {
                nextLevel = 8;
                return true;
            }
        }

        return false;
    }

    public void Setup()
    {
        obj = GameObject.FindGameObjectWithTag("ApolloInteraction");
        if (obj != null)
            apolloInteraction = obj.GetComponent<Lyre>();

        obj = GameObject.FindGameObjectWithTag("Apollo");
        if (obj != null)
            apollo = obj.GetComponent<Lyre>();

        obj = GameObject.FindGameObjectWithTag("Apollo2");
        if (obj != null)
            apollo2 = obj.GetComponent<Lyre>();

        obj = GameObject.FindGameObjectWithTag("AthenaInteraction");
        if (obj != null)
            athenaInteraction = obj.GetComponent<Olive>();

        obj = GameObject.FindGameObjectWithTag("Athena");
        if (obj != null)
            athena = obj.GetComponent<Olive>();

        obj = GameObject.FindGameObjectWithTag("Athena2");
        if (obj != null)
            athena2 = obj.GetComponent<Olive>();

        obj = GameObject.FindGameObjectWithTag("ZeusInteraction");
        if (obj != null)
            zeusInteraction = obj.GetComponent<Bolt>();

        obj = GameObject.FindGameObjectWithTag("Zeus");
        if (obj != null)
            zeus = obj.GetComponent<Bolt>();

        obj = GameObject.FindGameObjectWithTag("HermesInteraction");
        if (obj != null)
            hermesInteraction = obj.GetComponent<Caduceus>();

        obj = GameObject.FindGameObjectWithTag("Hermes");
        if (obj != null)
            hermes = obj.GetComponent<Caduceus>();

        obj = GameObject.FindGameObjectWithTag("Dialogue");
        if (obj != null)
            dialogue = obj.GetComponent<Dialogue>();

        obj = GameObject.FindGameObjectWithTag("Player");
        if (obj != null)
            player = obj.GetComponent<Arktos>();

        obj = GameObject.FindGameObjectWithTag("Burnable");
        if (obj != null)
            burnableTree = obj;
    }

    public void Startup()
    {
        start = false;
        lyreCount = 0;
        oliveCount = 0;
        caduceusCount = 0;
        boltCount = 0;

        collected = new List<Collectables>();
        burned = false;
        metApollo = false;
        metAthena = false;
        metHermes = false;
        metZeus = false;
    }
}
    