﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimMovement : MonoBehaviour
{
    public float speed;
    public float jumpSpeed;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Get the axis input
        float axis = Input.GetAxis("Horizontal");
        float hSpeed = Mathf.Abs(axis);

        //Move the sprite according to the input
        transform.Translate(new Vector3(axis * speed * Time.deltaTime, 0f, 0f));

        if (animator)
        {
            //Demetmine which way the sprite faces
            if (axis > 0.1)
                GetComponent<SpriteRenderer>().flipX = false;
            else if (axis < -0.1)
                GetComponent<SpriteRenderer>().flipX = true;

            //Triggers the run animation
            animator.SetFloat("Speed", hSpeed);

            if (Input.GetButton("Jump") == true)
            {
                //Triggers the jump animation if jump is pushed
                animator.SetBool("IsJump", true);
                GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpSpeed;
            }
            else
                animator.SetBool("IsJump", false);

            //Trigger death animation 
            if (Input.GetButton("Death")==true)
                animator.SetBool("IsDie", true);

            if(animator.GetBool("IsDie")== true)
            {
                if (Input.GetButton("Revive") == true)
                {
                    animator.SetBool("IsDie", false);
                }
            }
        }

        Debug.Log("Speed: " + axis);
    }
}
