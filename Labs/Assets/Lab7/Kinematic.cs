﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kinematic : MonoBehaviour
{
    private bool collison = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (collison)
        {
            transform.Translate(.1f, 0, 0);
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        collison = true;
        Debug.Log("Kinematic Collision!!!");
    }
}
