﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coroutines : MonoBehaviour
{
	public float speed;
	public List<Vector3> waypoints;
	public List<Color> colors;
	Coroutine path;

	void Start()
	{
		path = StartCoroutine(PatrolWaypoints());
		StartCoroutine(ColorShift());
	}

	private void Update()
	{
		if(Input.GetKey("space"))
		{
			StopCoroutine(path);
		}
	}

	public IEnumerator PatrolWaypoints()
	{
		while (true)
		{
			foreach (Vector3 point in waypoints)
			{
				while (transform.position != point)
				{
					transform.position = Vector3.MoveTowards(transform.position, point, speed);
					yield return null;
				}
			}
		}
	}

	public IEnumerator ColorShift()
	{
		SpriteRenderer shift = GetComponent<SpriteRenderer>();
		float time =+ 0.08f;
		while (true)
		{
			foreach (Color color in colors)
			{
				while (shift.color != color)
				{
					Debug.Log("Color shifting to " + color);
					shift.color = Color.Lerp(shift.color, color, time);
					yield return null;
				}
			}
		}
	}
}

