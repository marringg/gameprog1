﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class PlanetScript : MonoBehaviour
{
    private int count;
    public float move;
    public float rotation;

    // Start is called before the first frame update
    void Start()
    {
       //I don't think anything is needed here 
    }

    // Update is called once per frame
    void Update()
    {
        //defines the Vertical & Horisontal Move speed based on the overall move
        float verticalMove = move * Time.deltaTime * Input.GetAxis("Vertical"); 
        float horisontalMove = move* Time.deltaTime * Input.GetAxis("Horizontal");

        //using the new Vertiacal & Horisontal move speed translate the sprite with respect to the frame accordingly
        transform.Translate(horisontalMove, verticalMove, 0, Space.World);

        //Rotates the sprite on itself 
        transform.Rotate(Vector3.back * Time.deltaTime * rotation, Space.Self);

        count++;
        Debug.Log("Counter: " + count);
    }
}
