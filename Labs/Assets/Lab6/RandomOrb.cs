﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RandomOrb : MonoBehaviour
{
    private Vector3 mouseWPos;
    private Camera camera;
    public GameObject clickOrb;

    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //Gets the mouse postion 
        Vector3 worldPoint = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
        mouseWPos = new Vector3(worldPoint.x, worldPoint.y, 0);
        Debug.Log("Mouse: (" + Input.mousePosition.x + "," + Input.mousePosition.y + "," + Input.mousePosition.z + ")\n" +
           "MousePoint: " + worldPoint);


        if (Input.GetMouseButtonUp(0))
        {
            //Creates the object at the mouse postion
            GameObject obj = GameObject.Instantiate<GameObject>(clickOrb, mouseWPos, Quaternion.identity);
            Destroy(obj, 2);
        }

    }
}
