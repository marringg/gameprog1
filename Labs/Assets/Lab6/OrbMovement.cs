﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbMovement : MonoBehaviour
{

    float verticalSpeed, horisontalSpeed;
    // Start is called before the first frame update
    void Start()
    {
        //Creates random speed in both directions
        verticalSpeed = Random.Range(-10f, 10f)* Time.deltaTime;
        horisontalSpeed = Random.Range(-10f, 10f)* Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        //Moves the ball
        transform.Translate(horisontalSpeed, verticalSpeed, 0, Space.World);
    }
}
