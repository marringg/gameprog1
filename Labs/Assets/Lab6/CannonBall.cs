﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public float speed, gravity;
    public bool isRight = true;
    float vSpeed, hSpeed;

    // Start is called before the first frame update
    void Start()
    {
        //Math fo the correct velocity componets of a ball shot from a 45 degree angle
        vSpeed = (speed * (Mathf.Sqrt(2) / 2));
        hSpeed = (speed * (Mathf.Sqrt(2) / 2));
    }

    // Update is called once per frame
    void Update()
    {
        if (!isRight)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }

        //Establish movemnet speeds
        float verticalMove = vSpeed * Time.deltaTime;
        float horisontalMove = hSpeed* Time.deltaTime;

        //Correcting for gravity each update
        vSpeed = vSpeed - gravity;

        //Move the cannonball
        transform.Translate(horisontalMove, verticalMove, 0, Space.World);
    }
}
