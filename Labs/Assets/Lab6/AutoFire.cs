﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoFire : MonoBehaviour
{
    public GameObject cannonBall;
    private Animator animator;
    public Vector3 ballStart;
    // Start is called before the first frame update
    void Start()
    {
        ballStart = new Vector3(transform.position.x + 0.56f, transform.position.y + .75f, 0);

        //Auto Fire
        InvokeRepeating("ShootCannonBall", 2.0f, 2.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShootCannonBall()
    {
        GameObject obj = Instantiate<GameObject>(cannonBall, ballStart, Quaternion.identity);
        Destroy(obj, 1.5f);
    }

    public void switchBack(Object nullobj)
    {
        animator.SetBool("isFiring", false);
    }
}
