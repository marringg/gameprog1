﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class Cannon : MonoBehaviour
{
    public GameObject cannonBall;
    private Animator animator;
    public Vector3 ballStart;

    // Start is called before the first frame update
    void Start()
    {
        //Sets up animator
        animator = GetComponent<Animator>();

        //Sets the cannonball's postion relative to cannon
        ballStart = new Vector3(transform.position.x +0.56f, transform.position.y+.75f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //Fire Button
        if(Input.GetButton("Jump"))
        {
            animator.SetBool("isFiring", true);
        }
    }

    public void shootCannonBall(Object nullobj)
    {
        GameObject obj = GameObject.Instantiate<GameObject>(cannonBall, ballStart, Quaternion.identity);
        Destroy(obj, 1.5f);
    }

    public void switchBack(Object nullobj)
    {
        animator.SetBool("isFiring", false);
    }
}
