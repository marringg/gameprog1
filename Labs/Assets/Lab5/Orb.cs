﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{
    private float angle;
    public float speed = 50;
    public bool isClicked = false;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        angle = speed * Time.deltaTime;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).RotateAround(transform.position, Vector3.forward, angle);
        }

        if(isClicked)
        {
            transform.Translate(new Vector3(0.5f, 0, 0));
            isClicked = false;
        }
       
    }
}
