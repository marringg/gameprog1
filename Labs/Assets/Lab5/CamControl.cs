﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    public float move = 10;
    public float rotation = 10;
    private bool isRotate = false;
    private Orb blue, green, red, purple;
    private Vector3 mouseWPos;
    private Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        //Setting up objects for the Orbs
        GameObject obj = GameObject.FindGameObjectWithTag("Blue");
        blue = obj.GetComponent<Orb>();

        obj = GameObject.FindGameObjectWithTag("Green");
        green = obj.GetComponent<Orb>();

        obj = GameObject.FindGameObjectWithTag("Red");
        red = obj.GetComponent<Orb>();

        obj = GameObject.FindGameObjectWithTag("Purple");
        purple = obj.GetComponent<Orb>();
    }

    // Update is called once per frame
    void Update()
    {
        //Sets up camera for mouse input
        camera = Camera.main; 
        Vector3 worldPoint = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
        mouseWPos = new Vector3(worldPoint.x, worldPoint.y, 0);

        //Sets up keyboard input 
        float verticalMove = move * Time.deltaTime * Input.GetAxis("Vertical");
        float horisontalMove = move * Time.deltaTime * Input.GetAxis("Horizontal");

        //Uses keyboard input to move camera
        transform.Translate(horisontalMove, verticalMove, 0, Space.World);

        //Allows the camera to begin and stop rotating by pressing R
        if (Input.GetButton("Rotation") == true)
        {
            if (!isRotate)
            {
                isRotate = true;
            }
            else if (isRotate)
            {
                isRotate = false;
            }
        }

        if(isRotate)
        {
            transform.Rotate(Vector3.back * Time.deltaTime * rotation, Space.Self);
        }

        //Checks if mouse is in orb and if clicked
        if(blue.GetComponent<SpriteRenderer>().bounds.Contains(mouseWPos))
        {
            if (Input.GetMouseButton(0))
            {
                blue.isClicked = true;
            }
        }
        if(purple.GetComponent<SpriteRenderer>().bounds.Contains(mouseWPos))
        {
            if (Input.GetMouseButton(0))
            {
                purple.isClicked = true;
            }
        }
        if(red.GetComponent<SpriteRenderer>().bounds.Contains(mouseWPos))
        {
            if (Input.GetMouseButton(0))
            {
                red.isClicked = true;
            }
        }
        if(green.GetComponent<SpriteRenderer>().bounds.Contains(mouseWPos))
        {
            if (Input.GetMouseButton(0))
            {
                green.isClicked = true;
            }
        }


    }
}
