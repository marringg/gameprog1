﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigPlanet : MonoBehaviour
{
    private float angle;
    public float speed=50; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(transform.childCount);
        angle = speed * Time.deltaTime;
        
        transform.GetChild(0).RotateAround(transform.position, Vector3.forward, angle);
        
    }
}

