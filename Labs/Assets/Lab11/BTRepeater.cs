﻿using UnityEngine;
using System.Collections;
using UnityEditor.Animations;

public class BTRepeater : BTDecorator
{
    public BTRepeater(BehaviorTree t, BTNode c) : base(t, c)
    {

    }

    public override Result Execute()
    {
        Debug.Log("Child returned " + Child.Execute());
        return Result.Running;
    }
}
