﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorTree : MonoBehaviour
{
    private BTNode mRoot;
    private bool startedBehavior;
    private Coroutine behavior;
    public bool first;

    public Dictionary<string, object> Blackboard { get; set; }
    public BTNode Root { get { return mRoot;  } }

    void Start()
    {
        Blackboard = new Dictionary<string, object>();
        Blackboard.Add("WorldBounds", new Rect(0, 0, 5, 5));

        startedBehavior = false;

        if (first)
        {
            mRoot = new BTRepeatUntilFaliureNode(this, new BTSequencer(this, new BTNode[] {
                new BTRandomWalk(this) , new BTRandomWalk(this), new FailNode(this) }));
        }
        else
        {
            mRoot =  new BTSelectorNode(this, new BTNode[]
            { new BTRandomWalk(this), new BTRandomWalk(this), new FailNode(this), new BTRandomWalk(this), new FailNode(this), new BTRandomWalk(this)});
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!startedBehavior)
        {
            behavior = StartCoroutine(RunBehavior());
            startedBehavior = true;
        }
    }

    private IEnumerator RunBehavior()
    {
        BTNode.Result result = Root.Execute();
        while(result == BTNode.Result.Running)
        {
            Debug.Log("Root result: " + result);
            yield return null;
            result = Root.Execute();
        }
    }
}
