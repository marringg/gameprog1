﻿using UnityEngine;
using System.Collections;

public class BTRepeatUntilFaliureNode : BTRepeater
{
    public BTRepeatUntilFaliureNode(BehaviorTree t, BTNode c) : base(t, c)
    {

    }

    public override Result Execute()
    {
        Result result = Child.Execute();
        if(result!= Result.Failure)
            return Result.Running;
        else
        {
            return Result.Failure;
        }
    }
}

