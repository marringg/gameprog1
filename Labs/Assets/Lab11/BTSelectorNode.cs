﻿using UnityEngine;
using System.Collections;

public class BTSelectorNode : BTComposite
{
    int currentNode = 0;
    public BTSelectorNode(BehaviorTree t, BTNode[] children) : base(t, children)
    {

    }

    public override Result Execute()
    {
        if (currentNode < Children.Count)
        {
            Result result = Children[currentNode].Execute();
            
            if (result == Result.Running)
                return Result.Running;
            else
            {
                currentNode++;
                if (currentNode < Children.Count)
                    return Result.Running;
                else
                {
                    currentNode = 0;
                    return Result.Success;
                }
            }
        }
        return Result.Success;
    }
}