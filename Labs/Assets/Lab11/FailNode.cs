﻿using UnityEngine;
using System.Collections;

public class FailNode : BTNode
{
    public FailNode(BehaviorTree t) : base(t)
    {
    }

    public override Result Execute()
    {
        return Result.Failure;
    }
}