﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class Cat : MonoBehaviour
{
    GameManager manager;

    //Postion Variables
    public Vector3 startPoint = new Vector3(-4, 0,0);
    public Vector3 endPoint = new Vector3(4, 0, 0);
    private Vector3 currentPos;

    //Scale Variables
    Vector3 currentScale = new Vector3(1,1,1);
    Vector3 maxScale = new Vector3(3, 3, 1);
    Vector3 minScale = new Vector3(0, 0, 1);
    int direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        //Sets up the manager to get input from the gameManager object
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        manager = obj.GetComponent<GameManager>();

        //Sets currentPos to be the start
        currentPos = startPoint;
    }

    // Update is called once per frame
    void Update()
    {
        //Grabbing the values from the game manager
        float rotation = manager.rotationSpeed;
        float scale = manager.scalingSpeed;
        float move = manager.movementSpeed;
    
        //rotation
        transform.Rotate(Vector3.back * Time.deltaTime * rotation, Space.Self);

        //movement
        currentPos = Vector3.MoveTowards(currentPos, endPoint, move * Time.deltaTime);
        transform.position = currentPos;
        if (currentPos == endPoint)
        {
            //Switch end and start so the sprite changes movement direction
            Vector3 temp = startPoint;
            startPoint = endPoint;
            endPoint = temp;

            //Flips the actual direction of the sprite
            if (GetComponent<SpriteRenderer>().flipX == false)
                GetComponent<SpriteRenderer>().flipX = true;
            else
                GetComponent<SpriteRenderer>().flipX = false;
        }

        //Scaling
        //Direction is used to determine wheither the scale grows or shrinks the sprite
        if (currentScale.x>maxScale.x) 
        {
            direction = -1;
        }
        else if (currentScale.x < minScale.x)
        {
            direction = 1; 
        }

        currentScale = currentScale + new Vector3(direction*scale, direction*scale, 0) * Time.deltaTime;
        transform.localScale = currentScale;


    }
}
