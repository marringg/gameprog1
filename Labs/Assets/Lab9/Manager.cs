﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    //Prefab Characters
    public GameObject Wizard;
    public GameObject Ninja;
    public GameObject Fighter;

    //Toggle bools
    bool isWiz, isNin, isFight;

    //Input Variables
    string welcome;
    public Text textBox;

    //Button Method
    public void ButtonPress()
    {
        //Clears textbox
        textBox.text = "";

        //Determines which is toggled and creates the correct prefab
        if(isWiz)
        {
            GameObject obj = Instantiate<GameObject>(Wizard);
        }
        else if(isNin)
        {
            GameObject obj = Instantiate<GameObject>(Ninja);
        }
        else if (isFight)
        {
            GameObject obj = Instantiate<GameObject>(Fighter);
        }

        //Prints text
        StartCoroutine(Text());
    }

    //Toggle Methods
    public void WizToggle(bool value)
    {
        isWiz = value;
    }

    public void NinToggle(bool value)
    {
        isNin = value;
    }

    public void FightToggle(bool value)
    {
        isFight = value;
    }

    //Input Methods
    public void PlayerName(string name)
    {
        welcome = "Welcome Mighty " + name;
    }

    IEnumerator Text()
    {
        //Prints displayed name
        foreach (char letter in welcome)
        {
            textBox.text += letter;
            yield return new WaitForSeconds(.1f);
        }
        
    }
}
